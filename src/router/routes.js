const routes = [{
    path: "/",
    component: () => import("pages/Login.vue"),
    name: "login"
  },
  {
    path: "/welcome",
    name: "welcomeback",
    component: () => import("pages/Welcomeback.vue")
  },
  {
    path: "/test",
    name: "test",
    component: () => import("pages/template.vue")
  },

  {
    path: "/test2",
    component: () => import("pages/test2.vue"),
    name: "test2"
  },
  {
    path: "/printPrePostReport",
    component: () => import("pages/report/printPrePostReport.vue"),
    name: "printPrePostReport",
    props: true
  },
  // ปริ้นติดตามผลการเรียน
  {
    path: "printProgressMonitor",
    component: () => import("pages/monitor/printProgressMonitor.vue"),
    name: "printProgressMonitor",
    props: true
  },
  // หน้าปริ้นแบบฝึกหัด
  {
    // Flashcard
    path: "/practice/flashcard/print/",
    component: () => import("pages/practice/FlashcardPrint.vue"),
    name: "flashcardprint",
    props: true
  },
  {
    // Spelling bee
    path: "/practice/spellingbee/print/",
    component: () => import("pages/practice/SpellingbeePrint.vue"),
    name: "spellingbeeprint",
    props: true
  },
  {
    // Matching
    path: "/practice/matching/print/",
    component: () => import("pages/practice/MatchingPrint.vue"),
    name: "matchingprint",
    props: true
  },
  {
    // Mulitple choice
    path: "/practice/multiplechoice/print/",
    component: () => import("pages/practice/MultiplechoicePrint.vue"),
    name: "multiplechoiceprint",
    props: true
  },
  {
    // Grammar VDO Teaching
    path: "/practice/speaking/teachingvdo/print",
    component: () => import("pages/practice/VdotrainingPrint.vue"),
    name: "teachingvdoprint",
    props: true
  },
  {
    // Dictation
    path: "/practice/dictation/print/",
    component: () => import("pages/practice/DictationPrint.vue"),
    name: "dictationprint",
    props: true
  },
  {
    // Translation
    path: "/practice/translation/print/",
    component: () => import("pages/practice/TranslationPrint.vue"),
    name: "translationprint",
    props: true
  },
  {
    // Fill in the blank
    path: "/practice/fillintheblank/print/",
    component: () => import("pages/practice/FillintheblankPrint.vue"),
    name: "fillintheblankprint",
    props: true
  },
  {
    path: "/questionpool/print/:level/:unit",
    component: () => import("pages/practice/questionPoolPrint.vue"),
    name: "questionpoolprint"
  },
  {
    path: "/placementreportprint/:schoolcode/:academicyear/:classroom",
    component: () => import("pages/report/placementreportprint.vue"),
    name: "placementprint"
  },
  {
    // Reading Mulitple choice
    path: "/practice/readingmultiple/print/",
    component: () => import("pages/practice/ReadingmultiplePrint.vue"),
    name: "readingmultipleprint",
    props: true
  },
  {
    // Reading Fill in the blanks
    path: "/practice/readingfillin/print/",
    component: () => import("pages/practice/ReadingfillinPrint.vue"),
    name: "readingfillinprint",
    props: true
  },
  {
    // Conversation
    path: "/practice/conversation/print/",
    component: () => import("pages/practice/ConversationPrint.vue"),
    name: "conversationprint",
    props: true
  },
  {
    // ConversationForStudyplan
    path: "/practice/conversation/printstudy/:level/:unit",
    component: () => import("pages/practice/ConversationPrintstudy.vue"),
    name: "conversationprintstudy"
  },
  {
    // Speaking
    path: "/practice/speaking/print/",
    component: () => import("pages/practice/SpeakingPrint.vue"),
    name: "speakingprint",
    props: true
  },
  {
    path: "/",
    component: () => import("layouts/MyLayout.vue"),
    children: [{
        path: "mainmenu",
        component: () => import("pages/Mainmenu.vue"),
        name: "mainmenu"
      },
      {
        path: "/reportshow",

        component: () => import("pages/report/reportshow.vue"),
        name: "reportshow"
      },
      // จัดการเสียง
      {
        path: "sound",
        component: () => import("pages/practice/soundmain.vue"),
        name: "sound"
      },
      // จัดการ Level
      {
        path: "level",
        component: () => import("pages/practice/levelmain.vue"),
        name: "level"
      },
      {
        path: "level/add",
        component: () => import("pages/practice/leveladd.vue"),
        name: "leveladd"
      },
      {
        path: "level/edit/:id",
        component: () => import("pages/practice/leveladd.vue"),
        name: "leveledit"
      },

      //Question Pool
      {
        path: "questionpool",
        component: () => import("pages/practice/questionPoolMain.vue"),
        name: "questionpool"
      },
      {
        path: "questionpool/add",
        component: () => import("pages/practice/questionPoolAdd.vue"),
        name: "questionpooladd"
      },
      {
        path: "questionpool/edit/:id/:level/:unit",
        component: () => import("pages/practice/questionPoolAdd.vue"),
        name: "questionpooledit"
      },

      //Placement Test
      {
        path: "placement",
        component: () => import("pages/practice/placementTestMain.vue"),
        name: "placement"
      },
      {
        path: "placement/add",
        component: () => import("pages/practice/placementTestAdd.vue"),
        name: "placementadd"
      },
      {
        path: "placement/edit/:id",
        component: () => import("pages/practice/placementTestAdd.vue"),
        name: "placementedit"
      },

      // Pre-test / Post-test
      {
        path: "preposttest/",
        component: () => import("pages/practice/preposttestSetting.vue"),
        name: "preposttest"
      },
      {
        path: "preposttest/add",
        component: () => import("pages/practice/preposttestAdd.vue"),
        name: "preposttestadd"
      },
      {
        path: "preposttest/reset/:key/:code",
        component: () => import("pages/practice/preposttestReset.vue"),
        name: "preposttestreset"
      },

      // ประเภทโรงเรียน
      {
        path: "schooltype",
        component: () => import("pages/school/schooltypemain.vue"),
        name: "schooltype"
      },
      {
        path: "schooltype/add",
        component: () => import("pages/school/schooltypeadd.vue"),
        name: "schooltypeadd"
      },
      {
        path: "schooltype/edit/:id",
        component: () => import("pages/school/schooltypeadd.vue"),
        name: "schooltypeedit"
      },
      //โรงเรียน
      {
        path: "school",
        component: () => import("pages/school/Schoolmain.vue"),
        name: "school"
      },
      {
        path: "school/add",
        component: () => import("pages/school/Schooladd.vue"),
        name: "schooladd"
      },
      {
        path: "school/edit/:id",
        component: () => import("pages/school/Schooladd.vue"),
        name: "schooledit"
      },
      //ปีการศึกษา
      {
        path: "academicyear",
        component: () => import("pages/school/Academicyearmain.vue"),
        name: "academicyear"
      },
      {
        path: "academicyear/add/:id",
        component: () => import("pages/school/Academicyearadd.vue"),
        name: "academicyearadd"
      },
      {
        path: "academicyear/edit/:code/:id",
        component: () => import("pages/school/Academicyearadd.vue"),
        name: "academicyearedit"
      },
      //นักเรียน
      {
        path: "student",
        component: () => import("pages/user/Studentselect.vue"),
        name: "studentselect"
      },
      {
        path: "student/list",
        component: () => import("pages/user/Studentmain.vue"),
        name: "studentlist"
      },
      {
        path: "student/search",
        component: () => import("pages/user/Studentsearch.vue"),
        name: "studentsearch"
      },
      {
        path: "student/add",
        component: () => import("pages/user/Studentadd.vue"),
        name: "studentadd"
      },
      {
        path: "student/edit/:id",
        component: () => import("pages/user/Studentedit.vue"),
        name: "studentedit"
      },
      {
        path: "student/course/add/:studentid",
        component: () => import("pages/user/Studentaddcourse.vue"),
        name: "studentaddcourse"
      },
      {
        path: "student/course/edit/:studentid/:courseid",
        component: () => import("pages/user/Studentaddcourse.vue"),
        name: "studenteditcourse"
      },
      {
        path: "student/upload",
        component: () => import("pages/user/Studentupload.vue"),
        name: "studentupload"
      },
      {
        path: "student/changelevel/:classroom",
        component: () => import("pages/user/Studentchangelevel.vue"),
        name: "studentchangelevel"
      },
      //คุณครู
      {
        path: "teacher",
        component: () => import("pages/user/Teachermain.vue"),
        name: "teacher"
      },
      {
        path: "teacher/add",
        component: () => import("pages/user/Teacheradd.vue"),
        name: "teacheradd"
      },
      {
        path: "teacher/edit/:id",
        component: () => import("pages/user/Teacheradd.vue"),
        name: "teacheredit"
      },
      //ผู้ดูแลระบบ
      {
        path: "user",
        component: () => import("pages/user/Usermain.vue"),
        name: "User"
      },
      {
        path: "user/add",
        component: () => import("pages/user/Useradd.vue"),
        name: "useradd"
      },
      {
        path: "user/edit/:id",
        component: () => import("pages/user/Useradd.vue"),
        name: "useredit"
      },
      // รายงาน
      {
        path: "prepostreport",
        component: () => import("pages/report/prepostreport.vue"),
        name: "prepostreport"
      },
      {
        path: "placementreport",
        component: () => import("pages/report/placementreport.vue"),
        name: "placementreport"
      },
      {
        path: "jsonreport",
        component: () => import("pages/report/jsonreport.vue"),
        name: "jsonreport"
      },
      {
        path: "summarypractice",
        component: () => import("pages/report/summarypractice.vue"),
        name: "summarypractice"
      },
      //บิล
      {
        path: "bill",
        component: () => import("pages/etc/Bill.vue"),
        name: "bill"
      },
      {
        path: "bill/addSeller/",
        component: () => import("pages/etc/addSeller.vue"),
        name: "addSeller"
      },
      {
        path: "bill/editSeller/:id",
        component: () => import("pages/etc/addSeller.vue"),
        name: "editseller"
      },
      {
        path: "bill/addBill",
        component: () => import("pages/etc/addBill.vue"),
        name: "addBill"
      },
      {
        path: "bill/editBill/:id",
        component: () => import("pages/etc/addBill.vue"),
        name: "editBill"
      },
      //landing page
      {
        path: "landing/:page",
        component: () => import("pages/Landingpage.vue"),
        name: "landingpage"
      },
      // หน้าจัดการแบบฝึกหัด
      {
        path: "practice",
        component: () => import("pages/practice/PracticeMain.vue"),
        name: "practicemain"
      },
      {
        path: "practice/add/:level/:unit",
        component: () => import("pages/practice/PracticeAdd.vue"),
        name: "practiceadd"
      },
      // ย้ายแบบฝึกหัด
      {
        path: "practice/move/:key/:level/:unit",
        component: () => import("pages/practice/movePractice.vue"),
        name: "movepractice"
      },
      // หน้า Flashcard
      {
        path: "practice/flashcard/:key/:level/:unit/:settingmode/",
        component: () => import("pages/practice/Flashcard.vue"),
        name: "flashcard"
      },
      {
        path: "practice/flashcard/add/:key/:level/:unit/:settingmode/",
        component: () => import("pages/practice/FlashcardAdd.vue"),
        name: "flashcardadd"
      },
      {
        path: "practice/flashcard/edit/:key/:level/:unit/:practicekey/:settingmode/",
        component: () => import("pages/practice/FlashcardAdd.vue"),
        name: "flashcardedit"
      },

      // หน้า Spelling Bee
      {
        path: "practice/spellingbee/:key/:level/:unit",
        component: () => import("pages/practice/Spellingbee.vue"),
        name: "spellingbee"
      },
      // หน้า Matching
      {
        path: "practice/matching/:key/:level/:unit",
        component: () => import("pages/practice/Matching.vue"),
        name: "matching"
      },
      // หน้า Speaking
      {
        path: "practice/speaking/:key/:level/:unit",
        component: () => import("pages/practice/Speaking.vue"),
        name: "speaking"
      },
      // หน้า Fill in the blank
      {
        path: "practice/fillintheblank/:key/:level/:unit/:settingmode",
        component: () => import("pages/practice/Fillintheblank.vue"),
        name: "fillintheblank"
      },
      {
        path: "practice/fillintheblank/add/:key/:level/:unit/:settingmode/",
        component: () => import("pages/practice/FillintheblankAdd.vue"),
        name: "fillintheblankadd"
      },
      {
        path: "practice/fillintheblank/edit/:key/:level/:unit/:practicekey/:settingmode/",
        component: () => import("pages/practice/FillintheblankAdd.vue"),
        name: "fillintheblankedit"
      },

      // หน้า Dictation
      {
        path: "practice/dictation/:key/:level/:unit/:settingmode",
        component: () => import("pages/practice/Dictation.vue"),
        name: "dictation"
      },
      {
        path: "practice/dictation/add/:key/:level/:unit/:settingmode/",
        component: () => import("pages/practice/DictationAdd.vue"),
        name: "dictationadd"
      },
      {
        path: "practice/dictation/edit/:key/:level/:unit/:practicekey/:settingmode/:mode/",
        component: () => import("pages/practice/DictationAdd.vue"),
        name: "dictationedit"
      },
      // หน้า Translation
      {
        path: "practice/translation/:key/:level/:unit/:settingmode",
        component: () => import("pages/practice/Translation.vue"),
        name: "translation"
      },
      {
        path: "practice/translation/add/:key/:level/:unit/:settingmode/:mode",
        component: () => import("pages/practice/TranslationAdd.vue"),
        name: "translationadd"
      },
      {
        path: "practice/translation/edit/:key/:level/:unit/:practicekey/:settingmode/:mode/",
        component: () => import("pages/practice/TranslationAdd.vue"),
        name: "translationedit"
      },
      // หน้า Multiple choice
      {
        path: "practice/multiplechoice/:key/:level/:unit/:settingmode/",
        component: () => import("pages/practice/Multiplechoice.vue"),
        name: "multiplechoice"
      },
      {
        path: "practice/multiplechoice/add/:key/:level/:unit/:settingmode/",
        component: () => import("pages/practice/MultiplechoiceAdd.vue"),
        name: "multiplechoiceadd"
      },
      {
        path: "practice/multiplechoice/edit/:key/:level/:unit/:practicekey/:settingmode/",
        component: () => import("pages/practice/MultiplechoiceAdd.vue"),
        name: "multiplechoiceedit"
      },
      // หน้า Reading Fill in the blank
      {
        path: "practice/readingfillin/:key/:level/:unit/:settingmode",
        component: () => import("pages/practice/Readingfillin.vue"),
        name: "readingfillin"
      },
      {
        path: "practice/readingfillin/add/:key/:level/:unit/:settingmode/",
        component: () => import("pages/practice/ReadingfillinAdd.vue"),
        name: "readingfillinadd"
      },
      {
        path: "practice/readingfillin/edit/:key/:level/:unit/data/:settingmode/",
        component: () => import("pages/practice/ReadingfillinAdd.vue"),
        name: "readingfillinedit"
      },
      {
        path: "practice/readingfillin/add/question/:key/:level/:unit/:settingmode/",
        component: () => import("pages/practice/ReadingfillinQuestionAdd.vue"),
        name: "readingfillinquestionadd"
      },
      {
        path: "practice/readingfillin/edit/question/:key/:level/:unit/:practicekey/:settingmode/",
        component: () => import("pages/practice/ReadingfillinQuestionAdd.vue"),
        name: "readingfillinquestionedit"
      },
      // หน้า Reading Multiple Choice
      {
        path: "practice/readingmultiple/:key/:level/:unit/:settingmode",
        component: () => import("pages/practice/Readingmultiple.vue"),
        name: "readingmultiple"
      },
      {
        path: "practice/readingmultiple/add/:key/:level/:unit/:settingmode/",
        component: () => import("pages/practice/ReadingmultipleAdd.vue"),
        name: "readingmultipleadd"
      },
      {
        path: "practice/readingmultiple/edit/:key/:level/:unit/:practicekey/:settingmode/",
        component: () => import("pages/practice/ReadingmultipleAdd.vue"),
        name: "readingmultipleedit"
      },
      {
        path: "practice/readingmultiple/add/question/:key/:level/:unit/:settingmode/",
        component: () =>
          import("pages/practice/ReadingmultipleQuestionAdd.vue"),
        name: "readingmultiplequestionadd"
      },
      {
        path: "practice/readingmultiple/edit/question/:key/:level/:unit/:practicekey/:settingmode/",
        component: () =>
          import("pages/practice/ReadingmultipleQuestionAdd.vue"),
        name: "readingmultiplequestionedit"
      },
      //teaching vdo
      {
        path: "practice/teachingvdo/:key/:level/:unit/:settingmode",
        component: () => import("pages/practice/Vdotraining.vue"),
        name: "teachingvdo"
      },
      {
        path: "practice/teachingvdo/add/:key/:level/:unit/:slideno/:type",
        component: () => import("pages/practice/VdotrainingAdd.vue"),
        name: "teachingvdoadd"
      },
      {
        path: "practice/teachingvdo/edit/:key/:level/:unit/:slideno/:type",
        component: () => import("pages/practice/VdotrainingAdd.vue"),
        name: "teachingvdoedit"
      },
      //Error detection
      {
        path: "practice/errordetection/:key/:level/:unit/:settingmode",
        component: () => import("pages/practice/Errordetection.vue"),
        name: "errordetection"
      },
      {
        path: "practice/errordetection/add/:key/:level/:unit/:settingmode/",
        component: () => import("pages/practice/ErrordetectionAdd.vue"),
        name: "errordetectionadd"
      },
      {
        path: "practice/errordetection/edit/:key/:level/:unit/:practicekey/:settingmode/",
        component: () => import("pages/practice/ErrordetectionAdd.vue"),
        name: "errordetectionedit"
      },
      //Conversation
      {
        path: "practice/conversation/:key/:level/:unit/:settingmode",
        component: () => import("pages/practice/Conversation.vue"),
        name: "conversation"
      },
      {
        path: "practice/conversation/add/:key/:level/:unit/:type",
        component: () => import("pages/practice/ConversationAdd.vue"),
        name: "conversationadd"
      },
      {
        path: "practice/conversation/edit/:key/:level/:unit/:slideno/:type",
        component: () => import("pages/practice/ConversationAdd.vue"),
        name: "conversationedit"
      },
      // ระบบติดตามผลการเรียน
      {
        path: "monitor/progressMonitor",
        component: () => import("pages/monitor/progressMonitor.vue"),
        name: "progressmonitor"
      },
      // ระบบติดตามสถานะออนไลน์
      {
        path: "monitor/onlineStatus",
        component: () => import("pages/monitor/onlineStatus.vue"),
        name: "onlinestatus"
      }
    ]
  }
];
// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
