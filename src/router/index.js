import Vue from "vue";
import VueRouter from "vue-router";
import routes from "./routes";
import VueSweetalert2 from "vue-sweetalert2";
import vueXlsxTable from "vue-xlsx-table";
import vueNumeralFilterInstaller from "vue-numeral-filter";
import VueHtmlToPaper from "vue-html-to-paper";
import tinymce from "vue-tinymce-editor";
import VueLazyload from "vue-lazyload";
Vue.component("tinymce", tinymce);
Vue.use(VueSweetalert2);
Vue.use(vueXlsxTable, {
  rABS: false
}); //Browser FileReader API have two methods to read local file readAsBinaryString and readAsArrayBuffer, default rABS false
Vue.use(vueNumeralFilterInstaller);
Vue.use(VueRouter);
Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: "../assets/noimage.png",
  loading: "../assets/loading.gif",
  attempt: 1
});

const options = {
  name: "_blank",
  specs: ["fullscreen=yes"],
  styles: [
    "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
  ]
};

Vue.use(VueHtmlToPaper, options);

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

var config = {
  // apiKey: "AIzaSyBo9gzdKKj-uGBmjrEjEfijhQUD9rjYK_w",
  // authDomain: "test-81ed1.firebaseapp.com",
  // databaseURL: "https://test-81ed1.firebaseio.com",
  // projectId: "test-81ed1",
  // storageBucket: "test-81ed1.appspot.com",
  // messagingSenderId: "72916357835"
  apiKey: "AIzaSyAO6XZYR9cTvXCI-0orTpwlYm3Q2ys-FlY",
  authDomain: "winnerenglish-5f8d3.firebaseapp.com",
  databaseURL: "https://winnerenglish-5f8d3.firebaseio.com",
  projectId: "winnerenglish-5f8d3",
  storageBucket: "winnerenglish-5f8d3.appspot.com",
  messagingSenderId: "40516029824"
};

firebase.initializeApp(config);
export const db = firebase.firestore();

Vue.mixin({
  data() {
    return {
      appVersion: "1.7.24",
      vdoserverpath: "https://storage.googleapis.com/winnerenglish/upload/",
      serverpath: "https://api.winner-english.com/data/",
      monthOptions: [
        {
          label: "มกราคม",
          value: 1
        },
        {
          label: "กุมภาพันธ์",
          value: 2
        },
        {
          label: "มีนาคม",
          value: 3
        },
        {
          label: "เมษายน",
          value: 4
        },
        {
          label: "พฤษภาคม",
          value: 5
        },
        {
          label: "มิถุนายน",
          value: 6
        },
        {
          label: "กรกฎาคม",
          value: 7
        },
        {
          label: "สิงหาคม",
          value: 8
        },
        {
          label: "กันยายน",
          value: 9
        },
        {
          label: "ตุลาคม",
          value: 10
        },
        {
          label: "พฤศจิกายน",
          value: 11
        },
        {
          label: "ธันวาคม",
          value: 12
        }
      ],
      isShowmenu: false,
      numberOptions: [],
      tinyOptions: {
        menubar: false,
        force_br_newlines: true,
        force_p_newlines: false,
        forced_root_block: "",
        body_class: "#f00",
        height: 150
      }
    };
  },
  methods: {
    dateThaiFormat(data) {
      //เปลี่ยนการแสดงผลเป็น d/m/y
      let tempdata = data.split("-");
      return (
        tempdata[2] + "/" + tempdata[1] + "/" + (Number(tempdata[0]) + 543)
      );
    },

    convertslash(data) {
      //ทำการแปลง "/" เป็น "_"
      return data.replace("/", "_");
    },

    convertslashback(data) {
      //ทำการแปลง "_" เป็น "/"
      return data.replace("_", "/");
    },

    changeAcademicYear(data) {
      let tempdata = data.split("_");
      return tempdata[1] + "/" + tempdata[2];
    },

    thousandSeperator(data) {
      // ทำการแปลงตัวเลข ให้มี คอมม่าคั่นหลักพัน
      return data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },
    loopOptionsNumber(start, end) {
      let _this = this;
      let optionFinal = [];
      for (let i = start; i <= end; i++) {
        let dataFinal = {
          label: i.toString(),
          value: i
        };
        optionFinal.push(dataFinal);
      }
      _this.numberOptions = optionFinal;
    },
    notifyRed(messages) {
      this.$q.notify({
        color: "negative",
        position: "top",
        icon: "error",
        message: messages,
        timeout: 800
      });
    },
    notifyGreen(messages) {
      this.$q.notify({
        color: "secondary",
        position: "top",
        icon: "done",
        message: messages,
        timeout: 800
      });
    },
    removeP(text) {
      let find = "<p>";
      let find2 = "</p>";
      let reg1 = new RegExp(find, "g");
      let reg2 = new RegExp(find2, "g");
      let final = text.replace(reg1, "");
      final = final.replace(reg2, "<br>");
      return final;
    },
    loadingSet(type, messages) {
      if (type == 1) {
        this.$q.loading.show({
          message: messages,
          delay: 400 // ms
        });
      }

      if (type == 0) {
        setTimeout(() => {
          this.$q.loading.hide();
        }, 1000);
      }
    }
  }
});

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({
      y: 0
    }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  });

  return Router;
}
