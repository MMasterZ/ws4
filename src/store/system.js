const state = {
  userData: [], //ข้อมูล user
  userKey: 0, //user key ใช้ check การ login ซ้อน
  schoolcode: 0,
  academicyear: 0,
  linkCode: 'mainmenu',
  linkmenu: 2,
  levelactive: 1,
  unitactive: 1,
  practiceKey: "",
  SettingModeKey: "",
  classActive: "p1", // เก็บค่าวัดระดับ


}

const mutations = {
  setLinkCode: (state, payload) => { //Set ค่า userData
    state.linkCode = payload
  },
  setUserData: (state, payload) => { //Set ค่า userData
    state.userData = payload
  },
  setUserKey: (state, payload) => { //Set ค่า userKey
    state.userKey = payload
  },

  setSchoolCode: (state, payload) => {
    state.schoolcode = payload
  },
  setAcademicYear: (state, payload) => {
    state.academicyear = payload
  },
  setLevelActive: (state, payload) => {
    state.levelactive = payload
  },
  setUnitActive: (state, payload) => {
    state.unitactive = payload
  },
  setPracticeKey: (state, payload) => {
    state.practiceKey = payload
  },
  setSettingModeKey: (state, payload) => {
    state.settingModeKey = payload
  },
  setClassActive: (state, payload) => {
    state.classActive = payload
  },
  setLinkMenu: (state, payload) => {
    state.linkmenu = payload
  },

}

const actions = {
  setLinkCode: ({
    commit
  }, payload) => {
    commit('setLinkCode', payload)
  },
  setUserData: ({
    commit
  }, payload) => {
    commit('setUserData', payload)
  },
  setUserKey: ({
    commit
  }, payload) => {
    commit('setUserKey', payload)
  },

  setSchoolCode: ({
    commit
  }, payload) => {
    commit('setSchoolCode', payload)
  },
  setAcademicYear: ({
    commit
  }, payload) => {
    commit('setAcademicYear', payload)
  },
  setLevelActive: ({
    commit
  }, payload) => {
    commit('setLevelActive', payload)
  },
  setUnitActive: ({
    commit
  }, payload) => {
    commit('setUnitActive', payload)
  },
  setPracticeKey: ({
    commit
  }, payload) => {
    commit('setPracticeKey', payload)
  },
  setSettingModeKey: ({
    commit
  }, payload) => {
    commit('setSettingModeKey', payload)
  },
  setClassActive: ({
    commit
  }, payload) => {
    commit('setClassActive', payload)
  },
  setLinkMenu: ({
    commit
  }, payload) => {
    commit('setLinkMenu', payload)
  },
}

export default {
  state,
  mutations,
  actions
}
